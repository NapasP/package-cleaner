#!/usr/bin/env sh

set -e

for i in $(seq 1 100)
do
  version=$(echo "1k $i 10/p" | dc | awk '{printf "%.1f\n", $0}')
  echo "version: $version"
  minorVersion="$(echo "$version" | cut -d'.' -f1)"
  patchVersion="$(echo "$version" | cut -d'.' -f2)"
  npm version "1.$minorVersion.$patchVersion" --no-git-tag-version
  npm publish
done

for i in $(seq 1 50)
do
  version=$(echo "1k $i 10/p" | dc | awk '{printf "%.1f\n", $0}')
  devVersion=$(wget -qO- 'https://frightanic.com/goodies_content/docker-names.php' | sed 's/_/-/g')
  echo "version: $version-$devVersion"
  minorVersion="$(echo "$version" | cut -d'.' -f1)"
  patchVersion="$(echo "$version" | cut -d'.' -f2)"
  npm version "1.$minorVersion.$patchVersion-$devVersion.$((RANDOM%100000+100000))" --no-git-tag-version
  npm publish
done
